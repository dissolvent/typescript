interface IAttributes {
  [index: string]: string
}

interface IElement {
  tagName: string,
  className?: string,
  attributes?: IAttributes
}

export function createElement<T extends HTMLElement>({ tagName, className, attributes = {} }: IElement): T {
  const element = document.createElement(tagName) as T;

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
}
