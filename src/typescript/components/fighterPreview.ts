import { createElement } from '../helpers/domHelper';
import { IFighterDetails } from '../interfaces/fighters';

export function createFighterPreview(
  fighter: IFighterDetails,
  position: 'right' | 'left'): HTMLDivElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement<HTMLDivElement>({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  const imageElement = createFighterImage(fighter);
  fighterElement.append(imageElement);

  const fighterInfo = createFighterInfo(fighter);
  fighterElement.append(fighterInfo);
  return fighterElement;
}

export function createFighterImage(fighter: IFighterDetails): HTMLImageElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement<HTMLImageElement>({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}

export function createFighterInfo(fighter: IFighterDetails): HTMLDivElement {
  const infoContainerElement = createElement<HTMLDivElement>({
    tagName: 'div',
    className: 'fighter-preview___info'
  });

  const nameElement = createElement<HTMLHeadElement>({
    tagName: 'h2',
    className: 'fighter-preview___name'
  });
  const { name } = fighter;
  // ']' - underline for name
  nameElement.innerText = name + ']';
  infoContainerElement.append(nameElement);

  const fighterStatMap:Map<string, number> = new Map();
  fighterStatMap.set('health', fighter.health);
  fighterStatMap.set('attack', fighter.attack);
  fighterStatMap.set('defense', fighter.defense);
  fighterStatMap.forEach((statValue, statName) => {
    const statElement = createElement<HTMLParagraphElement>({
      tagName: 'p',
      className: `fighter-preview__${statName}`
    });
    statElement.innerText = `${statName}: ${statValue}`;
    infoContainerElement.append(statElement);
  });

  return infoContainerElement;
}
