import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { IFighterOverview } from '../interfaces/fighters';

export function createFighters(fighters: IFighterOverview[]): HTMLElement {
  const selectFighter = createFightersSelector();
  const container = createElement<HTMLDivElement>({ tagName: 'div', className: 'fighters___root' });
  const preview = createElement<HTMLDivElement>({ tagName: 'div', className: 'preview-container___root' });
  const fightersList = createElement<HTMLDivElement>({ tagName: 'div', className: 'fighters___list' });
  const fighterElements = fighters.map(fighter => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(
  fighter: IFighterOverview,
  selectFighter: (id: string) => Promise<void>
): HTMLDivElement {
  const fighterElement = createElement<HTMLDivElement>({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement = createImage(fighter);
  const onClick = () => selectFighter(fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: IFighterOverview): HTMLImageElement {
  const { source, name } = fighter;
  const attributes = { 
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement<HTMLImageElement>({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });

  return imgElement;
}