import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import { IFighterDetails } from '../../interfaces/fighters';

export function showWinnerModal(fighter: IFighterDetails): void {
  // call showModal function
  const { name } = fighter;
  const modalBody = createFighterImage(fighter);
  showModal({ title: `${name} Win!`, bodyElement: modalBody, onClose: () => location.reload() });
}
