import { controls } from '../../constants/controls';
import { CRITICAL_HIT_INACTIVE_DELAY } from '../../constants/delays';
import { IFighterDetails } from '../interfaces/fighters';

export async function fight([firstFighter, secondFighter]: IFighterDetails[]): Promise<IFighterDetails> {
  const playerOne = createPlayer(firstFighter, 'left', 'player one');
  const playerTwo = createPlayer(secondFighter, 'right', 'player two');

  const pressedKeys: Set<string> = new Set();

  const keyDownListener = (event: KeyboardEvent) => {
    keydownAction(event, pressedKeys, playerOne, playerTwo);
  };
  const keyUpListener = (event: KeyboardEvent) => {
    keyupAction(event, pressedKeys, playerOne, playerTwo);
  };

  document.addEventListener('keydown', keyDownListener);
  document.addEventListener('keyup', keyUpListener);
 
  return new Promise(resolve => {
    const battleOver = function (event: CustomEvent<player>): void {
      const winner = event.detail.playerNum === 'player one' ? playerOne : playerTwo;
      document.removeEventListener('keydown', keyDownListener);
      document.removeEventListener('keyup', keyUpListener);
      document.removeEventListener('winner', battleOver as EventListener);
      resolve(winner);
    };
    document.addEventListener('winner', battleOver as EventListener);
  });
}

interface IPlayerInFight {
  currentHealth: number;
  isBlocking: boolean;
  isCriticalHitAvailable: boolean;
  healthBar: HTMLElement;
  updateHealthBar(): void;
  playerNum: 'player one' | 'player two'
}

type player = IFighterDetails & IPlayerInFight;

function createPlayer(
  fighter: IFighterDetails,
  position: 'left' | 'right',
  playerNum: 'player one' | 'player two'
): player {
  const healthBarId = position === 'left' ? 'left-fighter-indicator' : 'right-fighter-indicator';
  const healthBar = document.getElementById(healthBarId) as HTMLDivElement;

  return {
    ...fighter,
    currentHealth: fighter.health,
    isBlocking: false,
    isCriticalHitAvailable: true,
    healthBar,
    playerNum,
    updateHealthBar: function() {
      const percentHp = (this.currentHealth / this.health) * 100;
      if (percentHp <= 0) {
        this.healthBar.style.width = '0%';
      } else {
        this.healthBar.style.width = percentHp.toFixed(2) + '%';
      }
    }
  };
}

function attack(attacker: player, defender: player) {
  if (!attacker.isBlocking && !defender.isBlocking) {
    defender.currentHealth -= getDamage(attacker, defender); 
    defender.updateHealthBar();
  }
  checkWinner(attacker, defender);
}

function isCriticalAttack(combination: string[], pressedKeys: Set<string>): boolean {
  return combination.every(it => pressedKeys.has(it));
}

function criticalAttack(attacker: player, defender: player) {
  if (!attacker.isCriticalHitAvailable) {
    return;
  }

  defender.currentHealth -= getCriticalHit(attacker);
  attacker.isCriticalHitAvailable = false;
  defender.updateHealthBar();
  setTimeout(() => (attacker.isCriticalHitAvailable = true), CRITICAL_HIT_INACTIVE_DELAY);
  checkWinner(attacker, defender);
}

function checkWinner(attacker: player, defender: player): void {
  if (defender.currentHealth <= 0) {
    document.dispatchEvent(new CustomEvent<player>('winner', { detail: attacker }));
  }
}

function keydownAction(
  event: KeyboardEvent,
  pressedKeys: Set<string>,
  playerOne: player,
  playerTwo: player
) {
  switch(event.code) {
    case(controls.PlayerOneAttack): {
      if (!event.repeat) {
        attack(playerOne, playerTwo);
      }
      break;
    }
    case(controls.PlayerTwoAttack): {
      if (!event.repeat) {
        attack(playerTwo, playerOne);
      }
      break;
    }
    case(controls.PlayerOneBlock): {
      playerOne.isBlocking = true;
      break;
    }
    case(controls.PlayerTwoBlock): {
      playerTwo.isBlocking = true;
      break;
    }
    default: {
      pressedKeys.add(event.code);
    }
  }

  if (isCriticalAttack(controls.PlayerOneCriticalHitCombination, pressedKeys)) {
    criticalAttack(playerOne, playerTwo);
  }

  if (isCriticalAttack(controls.PlayerTwoCriticalHitCombination, pressedKeys)) {
    criticalAttack(playerTwo, playerOne);
  }
}

function keyupAction(  
  event: KeyboardEvent,
  pressedKeys: Set<string>,
  playerOne: player,
  playerTwo: player
): void {
  switch(event.code) {
    case(controls.PlayerOneBlock): {
      playerOne.isBlocking = false;
      break;
    }
    case(controls.PlayerTwoBlock): {
      playerTwo.isBlocking = false;
      break;
    }
    default: {
      pressedKeys.delete(event.code);
    }
  }
}

export function getDamage(attacker: player, defender: player): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

export function getCriticalHit(attacker: player): number {
  return attacker.attack * 2;
}

export function getHitPower(fighter: player): number {
  return fighter.attack * getRandomFloat(1, 2);
}

export function getBlockPower(fighter: player): number {
  return fighter.defense * getRandomFloat(1, 2);
}

function getRandomFloat(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}
