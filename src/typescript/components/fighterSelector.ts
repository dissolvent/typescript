import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { IFighterDetails } from '../interfaces/fighters';

export function createFightersSelector(): (fighterId: string) => Promise<void> {
  const selectedFighters: IFighterDetails[] = [];

  return async (fighterId: string) => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne !== null && playerOne !== undefined ? playerOne : fighter;
    const secondFighter = playerOne
      ? playerTwo !== null && playerTwo !== undefined
        ? playerTwo : fighter 
      : playerTwo;

    if (firstFighter && !playerOne) {
      selectedFighters.push(firstFighter);
    }
    if (secondFighter) {
      selectedFighters.push(secondFighter);
    }
    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap:Map<string, IFighterDetails> = new Map();

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails | undefined> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
  if (!fighterDetailsMap.has(fighterId)) {
    const fighterDetails = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighterDetails);
  }

  return fighterDetailsMap.get(fighterId);
}

function renderSelectedFighters(selectedFighters: IFighterDetails[]) {
  const fightersPreview = document.querySelector('.preview-container___root') as HTMLDivElement;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: IFighterDetails[]) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement<HTMLDivElement>({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement<HTMLImageElement>({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg }
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement<HTMLButtonElement>({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: IFighterDetails[]) {
  renderArena(selectedFighters);
}
