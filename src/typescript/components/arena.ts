import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { IFighterDetails } from '../interfaces/fighters';

export function renderArena(selectedFighters: IFighterDetails[]): void {
  const root = document.getElementById('root') as HTMLDivElement;
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  fight(selectedFighters).then(showWinnerModal);
}

function createArena(selectedFighters: IFighterDetails[]): HTMLDivElement {
  const [firstFighter, secondFighter] = selectedFighters;
  const arena = createElement<HTMLDivElement>({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(firstFighter, secondFighter);
  const fighters = createFighters(firstFighter, secondFighter);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: IFighterDetails, rightFighter: IFighterDetails): HTMLDivElement {
  const healthIndicators = createElement<HTMLDivElement>({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement<HTMLDivElement>({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighterDetails, position: 'left' | 'right'): HTMLDivElement {
  const { name } = fighter;
  const container = createElement<HTMLDivElement>({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement<HTMLSpanElement>({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement<HTMLDivElement>({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement<HTMLDivElement>({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` }
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(firstFighter: IFighterDetails, secondFighter: IFighterDetails): HTMLDivElement {
  const battleField = createElement<HTMLDivElement>({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighterDetails, position: 'left' | 'right'): HTMLDivElement {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement<HTMLDivElement>({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
