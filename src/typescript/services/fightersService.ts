import { callApi } from '../helpers/apiHelper';
import { IFighterOverview, IFighterDetails } from '../interfaces/fighters';

class FighterService {
  async getFighters(): Promise<IFighterOverview[]> {
    const endpoint = 'fighters.json';
    const apiResult = await callApi<IFighterOverview[]>(endpoint, 'GET');
    return apiResult;
  }

  async getFighterDetails(id: string): Promise<IFighterDetails> {
    const endpoint = `details/figher/${id}.json`;
    const apiResult = await callApi<IFighterDetails>(endpoint, 'GET');
    return apiResult;
  }
}

export const fighterService = new FighterService();
